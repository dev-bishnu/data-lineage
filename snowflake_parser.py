from mo_sql_parsing import parse, format
from mo_parsing.exceptions import ParseException


class SnowflakeParser:

    def __init__(self, query: str, script_name: str):
        self.status = None
        self.script_name = script_name
        self.lineage = []

        try:
            parsed = parse(query, null=None)
            self.lineage = self.parse_query(parsed)

        except ParseException:
            self.status = 'UNPARSED'
        except:
            self.status = 'ERROR'

    def parse_query(self, query: dict):
        keys = list(query.keys())
        lineage = []

        if str(keys[0]).startswith('union'):
            # for q in query[keys[0]]:
            #     lineage.extend(self.parse_statement(q))
            for l in self.parse_statement(query[keys[0]][0]):
                lin = {'table': {l['table']}, 'table_src': {l['table_src']}, 'column': l['column'], 'source': {l['source']}}
                lineage.append(lin)
            for q in query[keys[0]][1:]:
                x = self.parse_statement(q)
                for i in range(len(x)):
                    if x[i]['column'] == lineage[i]['column']:
                        lineage[i]['source'].add(str(x[i]['source']))
                        lineage[i]['table'].add(x[i]['table'])
                        lineage[i]['table_src'].add(x[i]['table_src'])
            for i in range(len(lineage)):
                lineage[i]['source'] = '\n'.join(lineage[i]['source']).strip()
                lineage[i]['table'] = '\n'.join(lineage[i]['table']).strip()
                lineage[i]['table_src'] = '\n'.join(lineage[i]['table_src']).strip()
        else:
            pass
            lineage.extend(self.parse_statement(query))
        return lineage

    def parse_statement(self, query: dict):

        keys = list(query.keys())

        select = query[keys[0]]
        _from = query['from']

        columns = self.parse_select(select)
        try:
            tables = self.parse_from(_from)
        except:
            tables = []

        lineage = []
        for col in columns:
            if not tables:
                lineage.append({'table': '', 'table_src': '', **col})
                continue

            src = col['src']
            if not src:
                src = col['source']

            if src == 'null':
                lineage.append({'table': '', 'table_src': '', **col})
                continue

            try:
                int(src)
                lineage.append({'table': '', 'table_src': '', **col})
                continue
            except:
                pass

            src = src.split(',')
            if len(src) != 1:
                lineage.append({'table': '', 'table_src': '', **col})
                continue

            if len(tables) == 1:
                if not bool(tables[0]):
                    lineage.append({'table': '', 'table_src': '', **col})
                    continue
                lineage.append({**tables[0], **col})
                continue

            src_table = src[0].split('.')[0]
            for tbl in tables:
                if not bool(tbl):
                    continue
                if tbl['table'].endswith(src_table):
                    lineage.append({**tbl, **col})
                    break
            else:
                lineage.append({'table': '', 'table_src': '', **col})

        # if tables:
        #     print()
        #     print(self.script_name)
        #     for x in tables:
        #         print(x, bool(x))
        #
        # if lineage:
        #     print()
        #     print(self.script_name)
        #     for x in lineage:
        #         print(x)

        # return columns
        # return tables
        return lineage

    def parse_from(self, _from):
        tables = []

        if type(_from) is str:
            if '.' in str:
                _from = _from.split('.')[-1]
            pass
            tables.append({'table': _from, 'table_src': _from})

        elif type(_from) is dict:
            if 'value' in _from.keys():
                pass
                tables.append(self.get_table(_from))
            else:
                pass
                # Recursion

        else:
            pass
            for x in _from:
                if type(x) is str:
                    tables.append(self.get_table(x))
                else:
                    if 'value' in x.keys():
                        tables.append(self.get_table(x))
                    else:
                        tables.append(self.get_table(list(x.values())[0]))

        return tables

    def get_table(self, _from):
        table = _from['name']
        if type(_from['value']) is str:
            source = _from['value']
            if '.' in source:
                source = source.split('.')[-1]
            return {'table': table, 'table_src': source}
        # Recursion
        return {'table': table, 'table_src': ''}
        # return {}

    def parse_select(self, select):
        columns = []

        if type(select) is str:
            columns.append({'column': select, 'source': select, 'src': None})

        elif type(select) is dict:
            columns.append(self.get_column(select))

        else:
            for line in select:
                columns.append(self.get_column(line))

        return columns

    def get_column(self, line: dict):
        src = None

        if line['value'] is None:
            source = 'null'
        elif type(line['value']) is not dict:
            source = line['value']
        else:
            src = line['value']
            try:
                source = format(src)
            except:
                source = src

        column = source
        if 'name' in line.keys():
            if line['name'].strip() != '':
                column = line['name']

        if src:
            src = self.get_column_src(src)
        return {'column': column, 'source': source, 'src': src}

    def get_column_src(self, value: dict):
        if not value:
            return None

        src = list(value.values())[-1]

        if type(src) is int:
            return None

        if type(src) is str:
            if src.isupper():
                return src
            return None

        if type(src) is dict:
            if 'then' in src.keys():
                return src['then']
            return self.get_column_src(src)

        srcs = set()
        srcs.add(None)
        srcs.add('YYYY-MM-DD')
        for x in src:
            if type(x) is int:
                pass
            elif type(x) is str:
                if x.isupper():
                    srcs.add(x)
            elif type(x) is list:
                pass
            else:
                srcs.add(self.get_column_src(x))
        srcs.remove(None)
        srcs.remove('YYYY-MM-DD')
        return ','.join(srcs)
