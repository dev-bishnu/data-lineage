import os
import shutil
import pandas as pd

from snowflake_parser import SnowflakeParser

if not os.path.exists(f'{os.path.dirname(__file__)}/temp'):
    os.mkdir(f'{os.path.dirname(__file__)}/temp')

temp_dir = f'{os.path.dirname(__file__)}/temp'


class LogFileScanner:

    def __init__(self, output_dir: str, extraction_logs_dir='', load_logs_dir=''):
        self.output_dir = output_dir
        self.ext_logs_dir = extraction_logs_dir
        self.load_logs_dir = load_logs_dir

        self.lineage = {}
        self.no_extraction_files = []
        self.unparsed = []

        self.create_script_files()
        self.parse_scripts()
        self.export_lineage()

    def parse_scripts(self):
        print('Parsing scripts')

        os.chdir(temp_dir)
        for file in os.listdir()[:]:

            script_name = file[:-4]
            with open(f'{temp_dir}/{file}', 'r') as reader:

                stage = reader.readline().strip()
                script = reader.read()
                parser = SnowflakeParser(script, script_name)

                if parser.status == 'UNPARSED':
                    self.unparsed.append(script_name)
                    if stage not in self.lineage.keys():
                        self.lineage[stage] = {'script': script_name, 'error': 'Parsing Error'}
                elif parser.status == 'ERROR':
                    print(script_name)
                    self.lineage[stage] = {'script': script_name, 'error': 'Something Went Wrong'}
                else:
                    if not parser.lineage:
                        print(script_name)
                        self.lineage[stage] = {'script': script_name, 'error': 'Query too complex'}
                    elif stage in self.lineage.keys():
                        if 'error' in self.lineage[stage].keys():
                            self.lineage[stage]['lineage'] = parser.lineage
                            self.lineage[stage].pop('error')
                        elif 'lineage' in self.lineage[stage].keys():
                            if len(self.lineage[stage]['lineage']) == len(parser.lineage):
                                lin = self.lineage[stage]['lineage']
                                for z in range(len(lin)):
                                    temp = lin[z]['table'].split('\n')
                                    temp.extend(parser.lineage[z]['table'].split('\n'))
                                    temp = list(set(temp))
                                    lin[z]['table'] = '\n'.join(temp)

                                    temp = lin[z]['table_src'].split('\n')
                                    temp.extend(parser.lineage[z]['table_src'].split('\n'))
                                    temp = list(set(temp))
                                    lin[z]['table_src'] = '\n'.join(temp)
                                self.lineage[stage]['script'] += '\n' + script_name
                                self.lineage[stage]['lineage'] = lin
                    else:
                        self.lineage[stage] = {'script': script_name, 'lineage': parser.lineage}

        print()
        print('Scripts parsing completed')
        if self.unparsed:
            print(f'{len(self.unparsed)} files unparsed.')
        print()
        # if self.lineage:
        #     for i in self.lineage:
        #         print({i: self.lineage[i]})

    def export_lineage(self):
        dataframe = []
        for key, value in self.lineage.items():
            if 'error' not in value.keys():
                for x in value['lineage']:
                    dataframe.append([value['script'], key, x['table_src'], x['column'], x['source']])

        df = pd.DataFrame(dataframe)
        df.columns = ['Script Name', 'Stage Table Name', 'Table', 'Column', 'Column Source']

        df.to_excel(f'{self.output_dir}/data-lineage.xlsx', index=False)
        print(f'Data lineage exported to {self.output_dir}/data-lineage.xlsx')
        print(len(dataframe))
        # df.to_csv(f'{self.output_dir}/data-lineage.csv', index=False)
        # print(f'Data lineage exported to {self.output_dir}/data-lineage.csv')

    def create_script_files(self):
        print('Extracting scripts from logs')
        if os.path.exists(temp_dir):
            shutil.rmtree(temp_dir)
        os.mkdir(temp_dir)

        os.chdir(self.ext_logs_dir)
        for file in os.listdir()[:]:
            script_name = '_'.join(file.split('_')[:-1])

            with open(f'{self.ext_logs_dir}/{file}', 'r') as reader:
                content = reader.read()
                if '# Exporting Data #' not in content:
                    self.no_extraction_files.append(script_name)
                else:
                    stage = self.extract_stage(content)
                    script = self.extract_script(content)
                    self.lineage[script_name] = {'stage': stage}
                    with open(f'{temp_dir}/{script_name}.sql', 'w') as writer:
                        writer.write(f'{stage}\n')
                        writer.write(script)
        print()
        print(f'{len(self.no_extraction_files)} files does not have extraction scripts.')
        print(f'{len(self.lineage)} scripts extracted to temp folder.')
        print()

    @staticmethod
    def extract_stage(content: str):
        stage = content.split('# Exporting Data #')[1]
        stage = stage.split(': Data file:')[1].split('.txt')[0]
        stage = '_'.join(stage.split('_')[:-1]).strip()
        if stage.startswith('ht'):
            stage = stage.replace('ht_', '')
        stage = 'dwh_' + stage
        return stage

    @staticmethod
    def extract_script(content: str):
        script = content.split('# Exporting Data #')[1]
        script = script.split(': Data file:')[0]
        script = '\n'.join(script.split('\n')[1:-1])
        script = ':'.join(script.split(':')[3:])
        script = '\n'.join([line.strip() for line in script.split('\n')])
        return script

